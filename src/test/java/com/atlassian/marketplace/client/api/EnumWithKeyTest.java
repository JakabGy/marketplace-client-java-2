package com.atlassian.marketplace.client.api;

import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class EnumWithKeyTest
{
    @Test
    public void canGetAllValues()
    {
        assertThat(EnumWithKey.Parser.forType(MyEnum.class).getValues(),
            equalTo(new MyEnum[] { MyEnum.LITTLE, MyEnum.BIG }));
    }

    @Test
    public void valueForKeyMatchesKey()
    {
        assertThat(EnumWithKey.Parser.forType(MyEnum.class).safeValueForKey("big"),
            equalTo(Optional.of(MyEnum.BIG)));
    }

    @Test
    public void valueForKeyMatchesKeyCaseInsensitively()
    {
        assertThat(EnumWithKey.Parser.forType(MyEnum.class).safeValueForKey("BiG"),
            equalTo(Optional.of(MyEnum.BIG)));
    }
    
    @Test
    public void valueForKeyReturnsNoneIfNotFound()
    {
        assertThat(EnumWithKey.Parser.forType(MyEnum.class).safeValueForKey("medium"),
            equalTo(Optional.empty()));
    }
    
    @SuppressWarnings("unchecked")
    @Test(expected = IllegalStateException.class)
    public void cannotGetParserForUnsupportedEnum()
    {
        Class<?> badClassObscuredByTypeErasure = EnumWithoutKey.class;
        EnumWithKey.Parser.forType((Class<? extends EnumWithKey>) badClassObscuredByTypeErasure);
    }
    
    private static enum MyEnum implements EnumWithKey
    {
        LITTLE("little"),
        BIG("big");
        
        private final String key;
        
        private MyEnum(String key)
        {
            this.key = key;
        }
        
        public String getKey()
        {
            return key;
        }
    }
    
    private static enum EnumWithoutKey
    {
        RED,
        GREEN;
    }
}
