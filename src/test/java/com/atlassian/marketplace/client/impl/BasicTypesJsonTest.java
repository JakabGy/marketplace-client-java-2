package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.EnumWithKey;
import com.atlassian.marketplace.client.encoding.InvalidFieldValue;
import com.atlassian.marketplace.client.encoding.MissingRequiredField;
import com.atlassian.marketplace.client.model.HtmlString;
import com.atlassian.marketplace.client.model.ReadOnly;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import io.atlassian.fugue.Option;
import org.junit.Test;

import java.net.URI;

import static com.atlassian.marketplace.client.model.HtmlString.html;
import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.fail;

public class BasicTypesJsonTest extends BaseJsonTests
{
    @Test
    public void requiredPropertyIsDecoded() throws Exception
    {
        assertThat(decode("{\"x\":\"y\"}", HasRequiredProperty.class).x, equalTo("y"));
    }
    
    @Test
    public void errorIfRequiredPropertyIsOmitted() throws Exception
    {
        try
        {
            decode("{\"xxx\":1}", HasRequiredProperty.class);
            fail("expected exception");
        }
        catch (MpacException.InvalidResponseError e)
        {
            causeShouldBe(e, MissingRequiredField.class, some("missing required property \"x\" in HasRequiredProperty"));
        }
    }

    @Test
    public void requiredPropertyIsEncoded() throws Exception
    {
        HasRequiredProperty o = new HasRequiredProperty();
        o.x = "y";
        assertThat(encode(o), equalTo("{\"x\":\"y\"}"));
    }
    
    @Test
    public void optionalPropertyIsDecoded() throws Exception
    {
        assertThat(decode("{\"x\":\"y\"}", HasOptionalProperty.class).x, equalTo(some("y")));
    }

    @Test
    public void optionalPropertyCanBeOmitted() throws Exception
    {
        assertThat(decode("{\"xxx\":1}", HasOptionalProperty.class).x, equalTo(none(String.class)));
    }
    
    @Test
    public void optionalPropertyIsEncodedIfSome() throws Exception
    {
        HasOptionalProperty o = new HasOptionalProperty();
        o.x = some("y");
        assertThat(encode(o), equalTo("{\"x\":\"y\"}"));
    }

    @Test
    public void optionalPropertyIsNotEncodedIfNone() throws Exception
    {
        HasOptionalProperty o = new HasOptionalProperty();
        o.x = none();
        assertThat(encode(o), equalTo("{}"));
    }
    
    @Test
    public void listPropertyIsDecoded() throws Exception
    {
        assertThat(decode("{\"xs\":[\"y\",\"z\"]}", HasListProperty.class).xs, equalTo(ImmutableList.of("y", "z")));
    }
    
    @Test
    public void listPropertyIsEncoded() throws Exception
    {
        HasListProperty o = new HasListProperty();
        o.xs = ImmutableList.of("y", "z");
        assertThat(encode(o), equalTo("{\"xs\":[\"y\",\"z\"]}"));
    }

    @Test
    public void uriPropertyIsDecoded() throws Exception
    {
        assertThat(decode("{\"x\":\"http://foo/bar\"}", HasUriProperty.class).x,
            equalTo(URI.create("http://foo/bar")));
    }
    
    @Test
    public void errorIfUriIsMalformed() throws Exception
    {
        try
        {
            decode("{\"x\":\"::\"}", HasUriProperty.class);
            fail("expected error");
        }
        catch (MpacException.InvalidResponseError e)
        {
            causeShouldBe(e, InvalidFieldValue.class, some("\"::\" is not a valid value for URI"));
        }
    }
    
    @Test
    public void uriPropertyIsEncoded() throws Exception
    {
        HasUriProperty o = new HasUriProperty();
        o.x = URI.create("http://foo/bar");
        assertThat(encode(o), equalTo("{\"x\":\"http://foo/bar\"}"));
    }
    
    @Test
    public void enumPropertyIsDecoded() throws Exception
    {
        assertThat(decode("{\"color\":\"blu\"}", HasEnumProperty.class).color, equalTo(MyEnum.BLUE));
    }

    @Test
    public void errorIfEnumPropertyIsUnknownValue() throws Exception
    {
        try
        {
            assertThat(decode("{\"color\":\"blue\"}", HasEnumProperty.class).color, equalTo(MyEnum.BLUE));
            fail("expected error");
        }
        catch (MpacException.InvalidResponseError e)
        {
            causeShouldBe(e, InvalidFieldValue.class, some("\"blue\" is not a valid value for MyEnum"));
        }
    }
    
    @Test
    public void enumPropertyIsEncoded() throws Exception
    {
        HasEnumProperty o = new HasEnumProperty();
        o.color = MyEnum.GREEN;
        assertThat(encode(o), equalTo("{\"color\":\"gryn\"}"));
    }
    
    @Test
    public void htmlStringPropertyIsDecoded() throws Exception
    {
        assertThat(decode("{\"hs\":\"<b>hi</b>\"}", HasHtmlStringProperty.class).hs, equalTo(html("<b>hi</b>")));
    }
    
    @Test
    public void htmlStringPropertyIsEncoded() throws Exception
    {
        HasHtmlStringProperty o = new HasHtmlStringProperty();
        o.hs = html("<b>hi</b>");
        assertThat(encode(o), equalTo("{\"hs\":\"<b>hi</b>\"}"));
    }

    @Test
    public void uriMapPropertyIsDecoded() throws Exception
    {
        assertThat(decode("{\"uris\":{\"foo\":\"/url1\",\"bar\":\"/url2\"}}", HasUriMapProperty.class).uris,
                equalTo(ImmutableMap.of("foo", URI.create("/url1"), "bar", URI.create("/url2"))));
    }
    
    @Test
    public void uriMapPropertyIsEncoded() throws Exception
    {
        HasUriMapProperty o = new HasUriMapProperty();
        o.uris = ImmutableMap.of("foo", URI.create("/url1"), "bar", URI.create("/url2"));
        assertThat(encode(o), equalTo("{\"uris\":{\"foo\":\"/url1\",\"bar\":\"/url2\"}}"));
    }
    
    @Test
    public void readOnlyPropertyIsDecoded() throws Exception
    {
        assertThat(decode("{\"n\":1,\"s\":\"x\"}", HasReadOnlyProperty.class).s, equalTo("x"));
    }
    
    @Test
    public void readOnlyPropertyCanBeEncoded() throws Exception
    {
        HasReadOnlyProperty o = new HasReadOnlyProperty();
        o.n = 1;
        o.s = "x";
        assertThat(encode(o, true), equalTo("{\"n\":1,\"s\":\"x\"}"));
    }

    @Test
    public void readOnlyPropertyCanBeOmitted() throws Exception
    {
        HasReadOnlyProperty o = new HasReadOnlyProperty();
        o.n = 1;
        o.s = "x";
        assertThat(encode(o, false), equalTo("{\"n\":1}"));
    }

    static final class HasRequiredProperty
    {
        String x;
    }
    
    static final class HasOptionalProperty
    {
        Option<String> x;
    }
    
    static final class HasListProperty
    {
        ImmutableList<String> xs;
    }
    
    static final class HasUriProperty
    {
        URI x;
    }

    static final class HasHtmlStringProperty
    {
        HtmlString hs;
    }
    
    public static enum MyEnum implements EnumWithKey
    {
        BLUE("blu"),
        GREEN("gryn");
        
        private final String key;
        
        private MyEnum(String key)
        {
            this.key = key;
        }
        
        public String getKey()
        {
            return key;
        }
    }
    
    static final class HasEnumProperty
    {
        MyEnum color;
    }
    
    static final class HasUriMapProperty
    {
        ImmutableMap<String, URI> uris;
    }
    
    static final class HasReadOnlyProperty
    {
        int n;
        @ReadOnly String s;
    }
}
