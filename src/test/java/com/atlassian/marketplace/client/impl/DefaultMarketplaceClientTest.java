package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.ErrorDetail;
import com.atlassian.utt.matchers.NamedFunction;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.marketplace.client.TestObjects.BASE_URI;
import static com.atlassian.marketplace.client.impl.ClientTester.defaultRootResource;
import static com.atlassian.marketplace.client.model.TestModelBuilders.errorDetail;
import static com.atlassian.utt.reflect.ReflectionFunctions.accessor;
import static com.atlassian.utt.reflect.ReflectionFunctions.iterableAccessor;
import static io.atlassian.fugue.Option.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.verify;

public class DefaultMarketplaceClientTest
{   
    ClientTester tester;
    
    @Before
    public void setUp() throws Exception
    {
        tester = new ClientTester(BASE_URI);
        tester.mockResource(tester.apiBase, defaultRootResource());
    }

    @Test
    public void addonsAccessorQueriesRootResource() throws Exception
    {
        tester.client.addons();
        
        verify(tester.httpTransport).get(tester.apiBase);
    }
    
    @Test(expected=MpacException.class)
    public void addonsAccessorThrowsExceptionIfRootResourceNotAvailable() throws Exception
    {
        tester.mockResourceError(tester.apiBase, 404);
        tester.client.addons();
    }

    @Test
    public void addonsAccessorReturnsNonNullObject() throws Exception
    {
        assertThat(tester.client.addons(), not(nullValue()));
    }

    @Test
    public void applicationsAccessorQueriesRootResource() throws Exception
    {
        tester.client.applications();
        
        verify(tester.httpTransport).get(tester.apiBase);
    }
    
    @Test(expected=MpacException.class)
    public void applicationsAccessorThrowsExceptionIfRootResourceNotAvailable() throws Exception
    {
        tester.mockResourceError(tester.apiBase, 404);
        tester.client.applications();
    }

    @Test
    public void applicationsAccessorReturnsNonNullObject() throws Exception
    {
        assertThat(tester.client.applications(), not(nullValue()));
    }

    @Test
    public void productsAccessorQueriesRootResource() throws Exception
    {
        tester.client.products();
        
        verify(tester.httpTransport).get(tester.apiBase);
    }
    
    @Test(expected=MpacException.class)
    public void productsAccessorThrowsExceptionIfRootResourceNotAvailable() throws Exception
    {
        tester.mockResourceError(tester.apiBase, 404);
        tester.client.products();
    }

    @Test
    public void productsAccessorReturnsNonNullObject() throws Exception
    {
        assertThat(tester.client.products(), not(nullValue()));
    }
    
    @Test
    public void errorResponseWithNoBodyIsThrownWithJustStatusCode() throws Exception
    {
        tester.mockResourceError(tester.apiBase, 400);
        apiShouldFail(allOf(
            serverErrorStatus.is(400),
            serverErrorMessage.is("error 400"),
            serverErrorDetails.is(Matchers.<ErrorDetail>emptyIterable())
        ));
    }

    private void apiShouldFail(Matcher<MpacException.ServerError> errorConditions) throws Exception
    {
        try
        {
            tester.client.addons();
            Assert.fail("expected exception");
        }
        catch (MpacException.ServerError e)
        {
            assertThat(e, errorConditions);
        }
    }
    
    @Test
    public void errorResponseWithJsonBodyIsThrownWithStatusCodeAndDetails() throws Exception
    {
        ErrorDetail error1 = errorDetail("sorry");
        ErrorDetail error2 = errorDetail("unacceptable", some("/your/apology"), some("bad.thing")); 
        ImmutableList<ErrorDetail> details = ImmutableList.of(error1, error2);
        
        tester.mockResourceErrorBody(tester.apiBase, 400, details, "GET");
        
        apiShouldFail(allOf(
            serverErrorStatus.is(400),
            serverErrorMessage.is("400: sorry, /your/apology: unacceptable"),
            serverErrorDetails.is(contains(error1, error2))
        ));
    }

    @Test
    public void errorResponseWithNonJsonBodyIsThrownWithStatusCodeAndString() throws Exception
    {
        tester.mockResourceErrorBody(tester.apiBase, 400, "whatever", "GET");
        
        apiShouldFail(allOf(
            serverErrorStatus.is(400),
            serverErrorMessage.is("400: whatever"),
            serverErrorDetails.is(Matchers.<ErrorDetail>emptyIterable())
        ));
    }
    

    @Test
    public void errorResponseWithMalformedJsonBodyIsThrownWithStatusCodeAndString() throws Exception
    {
        tester.mockResourceErrorBody(tester.apiBase, 400, "{}", "GET");
        
        apiShouldFail(allOf(
            serverErrorStatus.is(400),
            serverErrorMessage.is("400: {}"),
            serverErrorDetails.is(Matchers.<ErrorDetail>emptyIterable())
        ));
    }
    
    private static NamedFunction<MpacException.ServerError, Integer> serverErrorStatus =
        accessor(MpacException.ServerError.class, int.class, "getStatus");
    
    private static NamedFunction<MpacException.ServerError, String> serverErrorMessage =
        accessor(MpacException.ServerError.class, String.class, "getMessage");
    
    private static NamedFunction<MpacException.ServerError, Iterable<ErrorDetail>> serverErrorDetails =
        iterableAccessor(MpacException.ServerError.class, ErrorDetail.class, "getErrorDetails");
}
