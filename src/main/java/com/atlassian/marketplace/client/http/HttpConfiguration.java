package com.atlassian.marketplace.client.http;

import io.atlassian.fugue.Option;
import org.apache.http.impl.client.ProxyAuthenticationStrategy;

import static com.google.common.base.Preconditions.checkNotNull;
import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.some;

/**
 * Encapsulates all the properties that control the client's HTTP behavior, except for the
 * base URI of the server.
 * @since 2.0.0
 */
public class HttpConfiguration
{
    public static final int DEFAULT_CONNECT_TIMEOUT_MILLIS = 15000;
    public static final int DEFAULT_READ_TIMEOUT_MILLIS = 15000;
    public static final int DEFAULT_MAX_CONNECTIONS = 10;
    public static final int DEFAULT_MAX_CACHE_ENTRIES = 100;
    public static final long DEFAULT_MAX_CACHE_OBJECT_SIZE = 60000;
    
    private final int connectTimeoutMillis;
    private final int readTimeoutMillis;
    private final int maxConnections;
    private final int maxCacheEntries;
    private final long maxCacheObjectSize;
    private final Option<Credentials> credentials;
    private final Option<Integer> maxRedirects;
    private final Option<ProxyConfiguration> proxy;
    private final Option<RequestDecorator> requestDecorator;
 
    /**
     * A username-password pair for client authentication or proxy authentication.
     */
    public static class Credentials
    {
        private final String username;
        private final String password;
        
        public Credentials(String username, String password)
        {
            this.username = checkNotNull(username, "username");
            this.password = checkNotNull(password, "password");
        }
        
        public String getUsername()
        {
            return username;
        }
        
        public String getPassword()
        {
            return password;
        }
        
        @Override
        public boolean equals(Object other)
        {
            if (other instanceof Credentials)
            {
                Credentials o = (Credentials) other;
                return o.username.equals(this.username) && o.password.equals(this.password);
            }
            return false;
        }
        
        @Override
        public int hashCode()
        {
            return username.hashCode() + password.hashCode();
        }
        
        @Override
        public String toString()
        {
            return "Credentials(" + username + ", " + password + ")";
        }
    }
    
    /**
     * Returns a new {@link Builder} for constructing an HttpConfiguration instance.
     */
    public static Builder builder()
    {
        return new Builder();
    }
    
    public static HttpConfiguration defaults()
    {
        return builder().build();
    }
    
    private HttpConfiguration(Builder builder)
    {
        this.connectTimeoutMillis = builder.connectTimeoutMillis;
        this.readTimeoutMillis = builder.readTimeoutMillis;
        this.maxConnections = builder.maxConnections;
        this.maxCacheEntries = builder.maxCacheEntries;
        this.maxCacheObjectSize = builder.maxCacheObjectSize;
        this.credentials = builder.credentials;
        this.maxRedirects = builder.maxRedirects;
        this.proxy = builder.proxy;
        this.requestDecorator = builder.requestDecorator;
    }
 
    public int getConnectTimeoutMillis()
    {
        return connectTimeoutMillis;
    }
    
    public int getReadTimeoutMillis()
    {
        return readTimeoutMillis;
    }
    
    public int getMaxConnections()
    {
        return maxConnections;
    }
    
    public int getMaxCacheEntries()
    {
        return maxCacheEntries;
    }
    
    public long getMaxCacheObjectSize()
    {
        return maxCacheObjectSize;
    }
    
    public Option<Integer> getMaxRedirects()
    {
        return maxRedirects;
    }
    
    public Option<Credentials> getCredentials()
    {
        return credentials;
    }
    
    public boolean hasCredentials()
    {
        return credentials.isDefined();
    }
    
    public Option<ProxyConfiguration> getProxyConfiguration()
    {
        return proxy;
    }
    
    public boolean hasProxy()
    {
        return proxy.isDefined();
    }

    public Option<RequestDecorator> getRequestDecorator()
    {
        return requestDecorator;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (other instanceof HttpConfiguration)
        {
            HttpConfiguration o = (HttpConfiguration) other;
            return o.credentials.equals(this.credentials) && o.proxy.equals(this.proxy) &&
                    o.requestDecorator.equals(this.requestDecorator) &&
                    o.connectTimeoutMillis == this.connectTimeoutMillis &&
                    o.readTimeoutMillis == this.readTimeoutMillis &&
                    o.maxCacheEntries == this.maxCacheEntries &&
                    o.maxCacheObjectSize == this.maxCacheObjectSize &&
                    o.maxConnections == this.maxConnections;
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return (int) (credentials.hashCode() + proxy.hashCode() + requestDecorator.hashCode() +
                connectTimeoutMillis + (int) readTimeoutMillis + maxCacheEntries + maxCacheObjectSize + maxConnections);
    }
    
    @Override
    public String toString()
    {
        return "HttpConfiguration(" + credentials + ", " + proxy + ", " + requestDecorator + ", " +
                connectTimeoutMillis + ", " + readTimeoutMillis + ", " + maxCacheEntries + ", " +
                maxCacheObjectSize + ", " + maxConnections + ")";
    }
    
    /**
     * Builder class for {@link HttpConfiguration}.  Use {@link HttpConfiguration#builder()} to create an instance. 
     */
    public static class Builder
    {
        private int connectTimeoutMillis = DEFAULT_CONNECT_TIMEOUT_MILLIS;
        private int readTimeoutMillis = DEFAULT_READ_TIMEOUT_MILLIS;
        private int maxConnections = DEFAULT_MAX_CONNECTIONS;
        private int maxCacheEntries = DEFAULT_MAX_CACHE_ENTRIES;
        private long maxCacheObjectSize = DEFAULT_MAX_CACHE_OBJECT_SIZE;
        private Option<Credentials> credentials = none();
        private Option<Integer> maxRedirects = none();
        private Option<ProxyConfiguration> proxy = none();
        private Option<RequestDecorator> requestDecorator = none();
        
        public HttpConfiguration build()
        {
            return new HttpConfiguration(this);
        }
        
        /**
         * Sets the length of time to wait for a connection before timing out.
         * @param connectTimeoutMillis  a number of milliseconds; null to use
         * {@link HttpConfiguration#DEFAULT_CONNECT_TIMEOUT_MILLIS}
         * @return  the same Builder
         */
        public Builder connectTimeoutMillis(Integer connectTimeoutMillis)
        {
            this.connectTimeoutMillis = (connectTimeoutMillis == null) ? DEFAULT_CONNECT_TIMEOUT_MILLIS :
                connectTimeoutMillis.intValue();
            return this;
        }
        
        /**
         * Sets the length of time to wait for a server response before timing out.
         * @param readTimeoutMillis  a number of milliseconds; null to use
         * {@link HttpConfiguration#DEFAULT_READ_TIMEOUT_MILLIS}
         * @return  the same Builder
         */
        public Builder readTimeoutMillis(Integer readTimeoutMillis)
        {
            this.readTimeoutMillis = (readTimeoutMillis == null) ? DEFAULT_READ_TIMEOUT_MILLIS :
                readTimeoutMillis.intValue();
            return this;
        }
        
        /**
         * Sets the maximum number of simultaneous HTTP connections the client can make to the server.
         * @param maxConnections  the maximum number of simultaneous connections; default is
         * {@link HttpConfiguration#DEFAULT_MAX_CONNECTIONS}
         * @return  the same Builder
         */
        public Builder maxConnections(int maxConnections)
        {
            this.maxConnections = maxConnections;
            return this;
        }
        
        /**
         * Sets the maximum number of HTTP responses that will be stored in the HTTP cache at a time.
         * @param maxCacheEntries  maximum number of cache entries; default is
         * {@link HttpConfiguration#DEFAULT_MAX_CACHE_ENTRIES}
         * @return  the same Builder
         */
        public Builder maxCacheEntries(int maxCacheEntries)
        {
            this.maxCacheEntries = maxCacheEntries;
            return this;
        }
        
        /**
         * Sets the maximum size of HTTP responses that can be stored in the HTTP cache.
         * @param maxCacheObjectSize  maximum cacheable object size in bytes; default is
         * {@link HttpConfiguration#DEFAULT_MAX_CACHE_OBJECT_SIZE}
         * @return  the same Builder
         */
        public Builder maxCacheObjectSize(long maxCacheObjectSize)
        {
            this.maxCacheObjectSize = maxCacheObjectSize;
            return this;
        }
        
        /**
         * Sets the maximum number of redirects that will be automatically followed.
         * @param maxRedirects  maximum number of redirects, if any
         * @return  the same Builder
         */
        public Builder maxRedirects(Option<Integer> maxRedirects)
        {
            this.maxRedirects = checkNotNull(maxRedirects);
            return this;
        }
        
        /**
         * Sets a username and password for basic authentication.  Authentication is not required for standard use of
         * the client; authenticating with a valid marketplace.atlassian.com account will allow you to see
         * unpublished versions of plugins you have permission to edit.
         * @param credentials  a {@link Credentials} object wrapped in {@link Option#some}; or {@link Option#none()}
         *   to turn off authentication
         * @return  the same Builder
         */
        public Builder credentials(Option<Credentials> credentials)
        {
            this.credentials = checkNotNull(credentials);
            return this;
        }
        
        /**
         * Sets HTTP proxy server parameters.  You do not have to do this if a proxy was already specified in standard
         * system properties supported by the JRE (<tt>http.proxyHost</tt>, etc.).  However, if you want to use proxy
         * authentication which the JRE does not support, you must supply a ProxyConfiguration.
         * @param proxy  a {@link ProxyConfiguration} object wrapped in {@link Option#some}, or {@link Option#none()} to
         *   use no proxy (unless one was already specified in system properties)
         * @return  the same Builder
         */
        public Builder proxyConfiguration(Option<ProxyConfiguration> proxy)
        {
            this.proxy = checkNotNull(proxy);
            return this;
        }
        
        /**
         * Specifies an object that can provide custom headers for HTTP requests, e.g. to set the User-Agent header.
         * @param requestDecorator  a {@link RequestDecorator} object wrapped in {@link Option#some}, or {@link Option#none()} for no decorator
         * @return  the same Builder
         */
        public Builder requestDecorator(Option<RequestDecorator> requestDecorator)
        {
            this.requestDecorator = checkNotNull(requestDecorator);
            return this;
        }
    }
    
    /**
     * Parameters for specifying an HTTP proxy.
     */
    public static class ProxyConfiguration
    {   
        private final Option<ProxyHost> proxyHost;
        private final Option<ProxyAuthParams> authParams;

        public static Builder builder()
        {
            return new Builder();
        }

        private ProxyConfiguration(Builder builder)
        {
            this.proxyHost = builder.proxyHost;
            this.authParams = builder.authParams;
        }

        public Option<ProxyHost> getProxyHost()
        {
            return proxyHost;
        }
        
        public boolean hasAuth()
        {
            return authParams.isDefined();
        }

        public Option<ProxyAuthParams> getAuthParams()
        {
            return authParams;
        }
                
        @Override
        public boolean equals(Object other)
        {
            if (other instanceof ProxyConfiguration)
            {
                ProxyConfiguration o = (ProxyConfiguration) other;
                return o.proxyHost.equals(this.proxyHost) && o.authParams.equals(this.authParams);
            }
            return false;
        }
        
        @Override
        public int hashCode()
        {
            return proxyHost.hashCode() + authParams.hashCode();
        }
        
        @Override
        public String toString()
        {
            return "ProxyConfiguration(" + proxyHost + ", " + authParams + ")";
        }
        
        /**
         * Builder class for {@link ProxyConfiguration}.
         */
        public static class Builder
        {
            private Option<ProxyHost> proxyHost = none();
            private Option<ProxyAuthParams> authParams = none();
            
            public ProxyConfiguration build()
            {
                return new ProxyConfiguration(this);
            }

            public Builder proxyHost(Option<ProxyHost> proxyHost)
            {
                this.proxyHost = checkNotNull(proxyHost);
                return this;
            }
            
            public Builder authParams(Option<ProxyAuthParams> authParams)
            {
                this.authParams = checkNotNull(authParams);
                return this;
            }            
        }
    }
    
    /**
     * Specifies the proxy hostname and port.  These are encapsulated in their own object, rather than being
     * required properties of {@link ProxyConfiguration}, because it is possible to specify other proxy
     * parameters explicitly ({@link ProxyAuthenticationStrategy}).
     */
    public static class ProxyHost
    {
        public static final int DEFAULT_PORT = 80;
        
        private final String hostname;
        private final int port;

        public ProxyHost(String hostname, int port)
        {
            this.hostname = checkNotNull(hostname);
            this.port = port;
        }
        
        public ProxyHost(String hostname)
        {
            this(hostname, 80);
        }
        
        public String getHostname()
        {
            return hostname;
        }
        
        public int getPort()
        {
            return port;
        }
        
        @Override
        public boolean equals(Object other)
        {
            if (other instanceof ProxyHost)
            {
                ProxyHost o = (ProxyHost) other;
                return o.hostname.equals(this.hostname) && (o.port == this.port);
            }
            return false;
        }
        
        @Override
        public int hashCode()
        {
            return hostname.hashCode() + port;
        }
        
        @Override
        public String toString()
        {
            return hostname + ":" + port;
        }
    }

    /**
     * Parameters for proxy authentication, instantiated only if you are using an authenticated proxy.
     */
    public static class ProxyAuthParams
    {
        private final Credentials credentials;
        private final ProxyAuthMethod authMethod;
        private final Option<String> ntlmDomain;
        private final Option<String> ntlmWorkstation;

        public ProxyAuthParams(Credentials credentials, ProxyAuthMethod authMethod, Option<String> ntlmDomain, Option<String> ntlmWorkstation)
        {
            this.credentials = checkNotNull(credentials, "credentials");
            this.authMethod = checkNotNull(authMethod, "authMethod");
            this.ntlmDomain = checkNotNull(ntlmDomain, "ntlmDomain");
            this.ntlmWorkstation = checkNotNull(ntlmWorkstation, "ntlmWorkstation");
        }
        
        public ProxyAuthParams(Credentials credentials, ProxyAuthMethod authMethod)
        {
            this(credentials, authMethod, none(String.class), none(String.class));
        }
        
        public Credentials getCredentials()
        {
            return credentials;
        }
        
        public ProxyAuthMethod getAuthMethod()
        {
            return authMethod;
        }
        
        public Option<String> getNtlmDomain()
        {
            return ntlmDomain;
        }
        
        public Option<String> getNtlmWorkstation()
        {
            return ntlmWorkstation;
        }
        
        @Override
        public boolean equals(Object other)
        {
            if (other instanceof ProxyAuthParams)
            {
                ProxyAuthParams o = (ProxyAuthParams) other;
                return o.credentials.equals(this.credentials) && o.authMethod.equals(this.authMethod)
                        && o.ntlmDomain.equals(this.ntlmDomain) && o.ntlmWorkstation.equals(this.ntlmWorkstation);
            }
            return false;
        }
        
        @Override
        public int hashCode()
        {
            return credentials.hashCode() + authMethod.hashCode() + ntlmDomain.hashCode() + ntlmWorkstation.hashCode();
        }
        
        @Override
        public String toString()
        {
            return "AuthParams(" + credentials + ", " + authMethod + ", " + ntlmDomain + ", " + ntlmWorkstation + ")";
        }
    }

    /**
     * Constants for the supported proxy authentication methods.
     */
    public enum ProxyAuthMethod
    {
        BASIC,
        DIGEST,
        NTLM;
        
        public static Option<ProxyAuthMethod> fromKey(String key)
        {
            for (ProxyAuthMethod a: values())
            {
                if (a.name().equalsIgnoreCase(key))
                {
                    return some(a);
                }
            }
            return none();
        }
    };
}
