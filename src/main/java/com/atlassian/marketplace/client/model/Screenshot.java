package com.atlassian.marketplace.client.model;

import io.atlassian.fugue.Option;

/**
 * One of the additional screenshots that can be part of {@link AddonVersion} details.
 * @since 2.0.0
 */
public final class Screenshot
{
    Links _links;
    @ReadOnly ScreenshotEmbedded _embedded;
    Option<String> caption;
    
    public Links getLinks()
    {
        return _links;
    }

    /**
     * The optional caption for the screenshot.
     */
    public Option<String> getCaption()
    {
        return caption;
    }
    
    /**
     * The full-sized image for the screenshot.
     */
    public ImageInfo getImage()
    {
        return _embedded.image;
    }

    static final class ScreenshotEmbedded
    {
        ImageInfo image;
    }
}
